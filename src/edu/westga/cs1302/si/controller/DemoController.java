package edu.westga.cs1302.si.controller;

import java.util.ArrayList;

import edu.westga.cs1302.si.model.Book;

/**
 * The Class DemoController.
 * 
 * @author CS1302
 */
public class DemoController {

	/**
	 * Demos functionality
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void run() {
		// Used by Task 1 of the SI exercise
		int sum = this.promptAndSumNumbersEnteredByUser();
		System.out.println("Sum of whole numbers entered by user is: " + sum);

		// Will be used as a part of Task 2 of the SI exercise
//		 ArrayList<Book> readingList = this.loadBooksFromFile("CSReadingList.dat");
//		 this.printBooks(readingList);
	}

	/**
	 * Prompts the user for integer numbers one by one and sums them until the user
	 * enters the sentinel value (-999). If the user enters an value that cannot be
	 * converted to an integer, e.g., "a". The program should inform the user, but
	 * continue to prompt the user for numbers to enter until -999 is entered. -999
	 * should not be part of the sum.
	 * 
	 * Example prompt: "Enter whole number: "
	 * 
	 * Hints: 
	 *    - Looping construct to use - conditional loop 
	 *    - Use the Scanner object with System.in to read input from the console. 
	 *    - Use exceptional handling to handle scenarios where user enters data that
	 *      cannot be converted to an int.
	 *    - Read in entire line and then use Integer.parseInt to parse the line that
	 *      was read in.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return The sum of the integers entered by the user.
	 */
	public int promptAndSumNumbersEnteredByUser() {
		// TODO: To be completed as part of Task 2
		
		return 0;
	}

	/**
	 * Loads the file specified by filename into a list of books and returns the
	 * list of books. The format of the file is a CSV file of the following format:
	 * <title>,<author>,<pages>,<price>
	 * 
	 * @precondition filename != null
	 * @postcondition none
	 * 
	 * @param filename
	 *            The file to load the books from.
	 * @return The list of books loaded from the file.
	 */
	public ArrayList<Book> loadBooksFromFile(String filename) {
		// TODO: To be completed as part of Task 2
		
		// TODO 1: Create and instatiate an ArrayList of Books
		
		// TODO 2: Create a File object from specified filename
		
		// TODO 3: Use a try-with resources to create a Scanner object, the catch 
		// block can just print out the stack trace
		
		// TODO 4: Within the try block do the following:
		//         a) Use the Scanner::useDelimiter method to set the delimiter for the fields
		//            to be a comma (,) OR \n OR \r\n.
		//         b) Loop while the scanner has a next line to read input from
		//         c) Within the loop use the Scanner::next method to read each of the four fields
		//            of the line one-by-one. The last two fields will need to be converted from a string to
		//            an integer (pages) and double (price), respectively.
		//		   d) Create a book object and add it to the book collection
		
		// TODO 5: Return the collection of books

		return null;
	}

	private void printBooks(ArrayList<Book> books) {
		for (Book currBook : books) {
			System.out.println(currBook);
		}
	}

}
